# Nexa Digital 2

## Stack

Esse projeto foi criado com base na Stack:
* NodeJs
* Express
* Nodemon
* App Engine Standard
E...
* Handlebars
* SASS
* JavaScript
* Gulp

## Estrutura do Projeto

* project root/

  * bin/ (entry point do servidor)
  * builds
    * dev
        * public
          * css/
          * js/
          * img/
    * prod
        * public (arquivos minificados, pré-processados, respondem pela pasta static em Produção)
          * css/  
          * js/
          * img/
  * components/
    * routes/
      * index.js (primeira url a ser iniciada), aqui podemos manipular o Handlebars para receber fetch do server
    * sass/
      * main.scss (arquivo que concatena os diversos arquios SASS e variáveis)
    * scripts/
      * lib/ (js criados para o projeto)
      * vendor (bibliotecas externas)
    * views/
      * [arquivos .hbs]
  * gulpfile.js
  * package.json

---
## Configurando o Ambiente de Desenvolvimento
1. Faça um Fork do repositório https://bitbucket.org/igqueiroz/nexa-digital-2/src/master/
2. Instale o Google SDK https://cloud.google.com/sdk/ para seu Sistema Operaciona
3. Configure sua conta
4. Instale o NodeJs https://nodejs.org/en/download/
5. Instale o nodemon ``` npm install -g nodemon ```
6. Instale o Ruby Gem e o SASS https://www.ruby-lang.org/pt/ se não tiver ainda
7. Instale o gulp ``` npm install -g gulp ```
8. Na pasta do repositório local rode o commando no seu terminal ``` npm install ``` e ``` npm run dev ``` ou ``` npm run dev:windows ``` para Windows
9. Ambiente Local do nodemon com hot refresh > http://localhost:8081/
10. Faça deploy na branch <development> e depois um pull request para a master seguindo o pipeline do BitBucket


----
## Testes Unitários
Fiz um teste com Jest para certificar que o Handlebars estava montando os componentes conforme o esperado
Os testes ficam localizados na pasta teste/App.test.js e faz parte de uma das etapas de Teste do Deploy.


---
## C/I C/D
* Para usar esse repositório você deve fazer um fork dele
* Use a branch de desenvolvimento <development>
* A branch master está blockeada, fazendo um Pull Request e um Merge o projeto é buildado no Pipeline do BitBucket e é feito o deploy em nuvem do App Engine
* O Pipeline está rodando em Tests, Staging e Production, pode ser encontrado no arquivo bitbucket-pipelines.yml
* O server do MongoLab está na pasta <server> do projeto, para atualizar o banco em ambiente de Prod, siga os passos:
1. Instale o SDK do Google Cloud
2. Peça permissão para seu usuário no (Projeto Nexa Digital)[https://console.cloud.google.com/home/dashboard?project=nexa-digital]
3. Abra seu terminal na pasta do server e execute o comando
  gcloud app deploy --version=principal

---
## Gulp
O Gulp é essencial para esse projeto, ele gera o build de todos os scripts e imagens para deploy.
Estou utilizando o novo Gulp 4, que mudou o jeito de escrever suas tasks com promisses e asyncs.
O Gulp está na raiz do projeto e suas tasks estão em: gulpfile.js

Para iniciar o gulp em ambiente de dev, abra o root do projeto e execute: 
``` ENV_MODE=dev gulp ```

ou para Windows
``` set ENV_MODE=dev&& gulp ```

Comandos:

* gulp watch

* gulp js

* gulp images

* gulp watch

* gulp (executa uma série de tasks)

* gulp build (vai para o pipeline do BiBucket)
