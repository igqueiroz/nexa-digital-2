const nconf = module.exports = require('nconf');
const path = require('path');
const env = (process.env.ENV_MODE === 'dev') ? "dev" : "prod" ;

var whitelist = [];

var baseUrl;

if (env === 'dev') {
  baseUrl = 'http://localhost:8081';
  whitelist = ['http://localhost:8081','http://127.0.0.1', 'http://localhost'];
} 

else {
  baseUrl = 'https://landing-dot-nexa-digital.appspot.com';
  whitelist = [''];
}

nconf
  // 1. Command-line arguments
  .argv()
  // 2. Defaults
  .defaults({
    ENV: env,
    DATA_URL: baseUrl,
    DATA_WHITELIST: whitelist,
  });