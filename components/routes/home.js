const express = require('express');
const router = express.Router();
const config = require('../../config');
const baseUrl = config.get('DATA_URL');
const env = config.get('ENV');

/* GET the admin page. */
router.get('/', function(req, res) {
	var model = {
		title: 'Landing Dreamshare',
		baseUrl: baseUrl,
		devMode: (env === 'dev') ? true : false,
		steps: [{
			image: 'step1.jpg',
			number: 'Step 1',
			title: 'Sed leo enim, condimentum',
			text: 'Quisque libero libero, dictum non turpis in, luctus semper lorem. Donec rhoncus a leo sit amet facilisis.'
		}, {
			image: 'step2.jpg',
			number: 'Step 2',
			title: 'Sed leo enim, condimentum',
			text: 'Quisque libero libero, dictum non turpis in, luctus semper lorem. Donec rhoncus a leo sit amet facilisis.'
		}, {
			image: 'step3.jpg',
			number: 'Step 3',
			title: 'Sed leo enim, condimentum',
			text: 'Quisque libero libero, dictum non turpis in, luctus semper lorem. Donec rhoncus a leo sit amet facilisis.'
		}],
		partners: [{
			image: 'bradley.png',
			image_tile: 'Bradley',
			background: '#ff4e50',
			icon: 'music.svg',
			icon_title: 'Music',
			name: 'Bradley Hunter',
			text: 'Based in Chicago. I love playing tennis and loud music.'

		}, {
			image: 'marie.png',
			image_tile: 'Marie',
			background: '#19d4ca',
			icon: 'brush.svg',
			icon_title: 'Brush',
			name: 'Marie Bennett',
			text: 'Currently living in Colorado. Lover of art, languages and travelling.'
		}, {
			image: 'diana.png',
			image_tile: 'Diana',
			background: '#ffa710',
			icon: 'camera.svg',
			icon_title: 'Camera',
			name: 'Diana Wells',
			text: 'Living in Athens, Greece. I love black and white classics, chillout music and green tea.'
		}, {
			image: 'cristopher.png',
			image_tile: 'Christopher',
			background: '#667df3',
			icon: 'airplane.svg',
			icon_title: 'Airplane',
			name: 'Christopher Pierce<',
			text: 'Star Wars fanatic. I have a persistent enthusiasm to create new things.'
		}]

	};
res.render('index', model);
});

module.exports = router;