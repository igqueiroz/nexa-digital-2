// These are the Node.js modules used for this Express app.
// They are installed using NPM
const express = require('express');
const cors = require("cors");
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const hbs = require('express-handlebars');

// These are the modules that we write.
// They are specific to our application.
const index = require('./components/routes/index');
const reload = require('reload');

// This creates the Express app which is configured below.  
const app = express();
reload(app);

const options = {};
// const bundler = new Bundler(file, options);

// app.use(bundler.middleware());
// Set up the view engine.
// Views are in a folder called "views"
// Handlebars (hbs) is used for the templating engine.
app.set('views', path.join(__dirname, 'components/views'));
app.set('view engine', 'hbs');
app.engine('hbs', hbs({
    extname: 'hbs', 
    defaultLayout: 'layout', 
    layoutsDir: path.join(__dirname, 'components/views'),
    partialsDir  : [
        //  path to your partials
        path.join(__dirname, 'components/partials'),
    ]
}));

// Standard Express stuff
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());

if (process.env.ENV_MODE === 'dev') {
	app.use('/static', express.static(__dirname + '/builds/dev/public'))
} else {
	app.use('/static', express.static(__dirname + '/builds/prod/public'))
}

// This configures the routes.
// Requests for the root folder are handled by the index module.
// Requests for the /api/conversions route are handle by the 
// conversions module.
// These modules were added above.
app.use('/', index);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('ENV_MODE') === 'dev' ? err : 'Oooops... página não encontrada';

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// Important later!  This exports the app object as a module.
// This comes into play when we deploy the application to 
// Cloud Functions.
module.exports = app;