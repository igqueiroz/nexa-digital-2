const config = require('./config');
const env = config.get('ENV');
const gulp = require('gulp'),
	  gutil = require('gulp-util'),
	  concat = require('gulp-concat'),
	  compass = require('gulp-compass'),
	  connect = require('gulp-connect'),
	  gulpif = require('gulp-if'),
	  uglify = require('gulp-uglify'),
	  jsonminify = require('gulp-jsonminify'),
	  imagemin = require('gulp-imagemin'),
	  pngcrush = require('imagemin-pngcrush')

var jsSources,
    sassSources,
    jsonSources,
    outputDir,
    sassStyle;

console.log(env);
if (env ==='dev') {
	outputDir = 'builds/dev/public/';
	sassStyle = 'expanded';
} else {
	outputDir = 'builds/prod/public/';
	sassStyle = 'compressed';
}

jsSources = [
	'components/scripts/vendor/jquery-3.3.1.js',
	'components/scripts/vendor/jquery.mask.js',
	'components/scripts/vendor/jquery.ui.widget.js',
	'components/scripts/vendor/popper.js',
	'components/scripts/vendor/bootstrap.js',
	'components/scripts/vendor/axios.js',
	'components/scripts/vendor/flickity.js',
	'components/scripts/lib/*.js'
]


sassSources = [
	'components/sass/main.scss'
]

jsonSources = [outputDir + 'js/*.json'];

gulp.task('js', function(done) {
	gulp.src(jsSources)
		.pipe(concat('scripts.js'))
		.pipe(gulpif(env === 'prod', uglify()))
    	.pipe(gulp.dest(outputDir + 'js'))
		.pipe(gulp.dest('builds/dev/public/js'))
		.pipe(connect.reload())
	done();
});

gulp.task('compass', function(done) {
	gulp.src(sassSources)
		.pipe(compass({
			sass: 'components/sass',
	        image: outputDir + 'img',
	        style: sassStyle
		}))
		.on('error', gutil.log)
	    .pipe(gulp.dest(outputDir + 'css'))
	    .pipe(connect.reload())
	done();
});

gulp.task('watch', function() {
	gulp.watch(jsSources, gulp.series('js'));
	gulp.watch('components/sass/*.scss', gulp.series('compass'));
	gulp.watch('builds/dev/public/js/*.json', gulp.series('json'));
	gulp.watch('builds/dev/public/img/**/*.*', gulp.series('images'));
});


gulp.task('images', function(done) {
  gulp.src('builds/dev/public/img/**/*.*')
    .pipe(gulpif(env === 'prod', imagemin({
      progressive: true,
      svgoPlugins: [{ removeViewBox: false }],
      use: [pngcrush()]
    })))
    .pipe(gulpif(env === 'prod', gulp.dest(outputDir + 'img')))
    .pipe(connect.reload())
    done()
});

gulp.task('json', function(done) {
  gulp.src('builds/dev/public/js/*.json')
    .pipe(gulpif(env === 'prod', jsonminify()))
    .pipe(gulpif(env === 'prod', gulp.dest('builds/prod/public/js')))
    .pipe(connect.reload())
    done()
});

gulp.task('connect', function(done) {
	connect.server({
		root: 'app',
		livereload: true
	})
	done()
});

gulp.task('build', gulp.series(['json', 'js', 'compass', 'images']));

gulp.task('default', gulp.series(['json', 'js', 'compass', 'images', 'connect', 'watch']));